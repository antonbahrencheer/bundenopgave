@extends('base')

@section('page')
    @include('pages.header')
    <div id="content">
        <div class="container padding-top-page">
        <div class="row">
            <div class="col-lg-4">
                <div style="padding-top: 25px;">

                <affix class="menu sidebar-menu vue-affix affix" relative-element-selector="#thisbox" style="width: 320px" :offset="{ top: 120, bottom: 100 }">
                    <scrollactive :offset="140" class="my-nav">
                        <ul>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-1">Hvem står bag det Danske Nummer?</a></li>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-2">Hvem svarer, når der ringes op fra et andet land?</a></li>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-3">Hvad koster det mig?</a></li>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-4">Bliver mit telefon nummer gjort offentligt og delt?</a></li>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-5">Hvad nu hvis der ikke er en dansker, der har tid til at svare?</a></li>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-6">Hvordan skal jeg svare?</a></li>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-7">Tidzoner – betyder det noget?</a></li>
                            <li><a class="scrollactive-item" v-smooth-scroll="{ duration: 1500, offset: -140 }" href="#markup-8">Manger du svar på et helt andet spørgsmål?</a></li>
                        </ul>
                    </scrollactive>
                </affix>
                </div>
            </div>
            <div class="col-lg-8" id="thisbox">
                <section id="markup-1">
                    <h2>Hvem står bag det Danske Nummer?</h2>
                    <p>Det er Wonderful Denmark, der står bagved ”Ring til en Dansker”.
Meningen er, at sparke til andre menneskers nysgerrighed om Danmark – kulturen, naturen og vores verdenssyn. ”Ring til en dansker” er også en hyldest til vores ytringsfrihed og alle danskeres engagement i det danske samfund. Ideen er, at alle danske får muligheden for at svare på Danmarks vegne.</p>
                </section>

                <section id="markup-2">
                    <h2>Hvem svarer, når der ringes op fra et andet land?</h2>
                    <p>Alle der bor i Danmark kan registrere sig som ambassadør og kan så blive ringet op. Når en person fra et andet land ringer, vil de automatisk blive omstillet til en tilfældig dansker (der er tilmeldt og registreret). Chancen for, at du som ”ring til en dansker” person bliver ringet op af den samme person flere gange er meget meget lille. Så der vil altid være en ny side af Danamark, som den der ringer op kan opleve.</p>
                </section>

                <section id="markup-3">
                    <h2>Hvad koster det mig?</h2>
                    <p>For dig som svarperson vil det ikke koste noget, da det er den anden, der ringe op. Men den der ringer til nummeret vil blive afkrævet de normale takster for internationale samtaler, som pågældendes teleselskab kræver.</p>
                </section>

                <section id="markup-4">
                    <h2>Bliver mit telefon nummer gjort offentligt og delt?</h2>
                    <p>Nej, dit telefonnummer er fuldstændigt anonymt. Alle opkald kommer gennem et automatisk omstillingsbord.</p>
                </section>

                <section id="markup-5">
                    <h2>Hvad nu hvis der ikke er en dansker, der har tid til at svare?</h2>
                    <p>Der kan være flere opkald end der er danskere til rådighed – derfor vil vi gerne have så mange som muligt til at registrere sig ;-)
Tidszoner kan jo betyde, at vi alle sover trygt, når en person fra den anden side af jorden ringer
Hvis den første ledige dansker ikke svarer, så vil der automatisk blive stillet om til den næste ledige – indtil der er en, som svarer
Og så er alle danskere jo til bål den 23. juni (Sankt Hansaften), så der er nok ikke nogen, der har tid til at svarer den aften.</p>
                </section>

                <section id="markup-6">
                    <h2>Hvordan skal jeg svare?</h2>
                    <p>Det bestemmer du selv – men et godt dansk ”goddag” eller ”Hej” vil være et sted at starte</p>
                </section>

                <section id="markup-7">
                    <h2>Tidzoner – betyder det noget?</h2>
                    <p>Ja, selvfølgelig betyder tidszonerne noget. Derfor kan du måske risikerer, at der er en fra Samoa, der ringer kl. 4 om natten. Men husk så bare, at de er 11 timer foran os og prøv at være høflig, når din telefon vækker dig med et spørgsmål om, hvad der smukkest i Danmark lige nu.</p>
                </section>

                <section id="markup-8">
                    <h2>Manger du svar på et helt andet spørgsmål?</h2>
                    <p>Hvis du har et spørgsmål, som ikke står her, så bare skriv til:
info@ringtilendansker.dk</p>
                </section>
            </div>
            </div>
        </div>
        @include('pages.footer')
    </div>
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@endpush