<header class="main-header">
    <div class="upper-accent"></div>
    <div class="container">
        <a href="/" class="logo"><img src="resources/logo.svg"></a>
        {{-- <nav class="menu-wrap">
            <ul class="main-menu">
                <li><a href="/ring-til-danmark">Ring til Danmark</a></li>
                <li><a href="/download">Download app</a></li>
                <li><a href="/faq">FAQ</a></li>
            </ul>
        </nav> --}}

        <nav class="navbar navbar-expand-lg">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse menu-wrap" id="navbarNavAltMarkup">
                <div class="navbar-nav main-menu">
                    <a href="/" class="nav-item nav-link ">Ring til Danmark</a>
                    <a href="/download" class="nav-item nav-link ">Download app</a>
                    <a href="/faq" class="nav-item nav-link ">FAQ</a>
                </div>
            </div>
        </nav>
    </div>
</header>