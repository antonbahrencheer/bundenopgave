@extends('base')

@section('page')
    <div id="content">
        <section class="download-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <img src="resources/iphone.png" class="iphone" alt="">
                    </div>

                    <div class="col-lg-6 col-lg-offset-1 app-description">
                        <div class="desc-container">
                            <h1>Svar for Danmark</h1>
                            <span class="seperator"></span>
                            <p>Danmark er et af de første lande i verden, der har fået sit eget telefonnummer, som hele verden kan ringe på og vi, der bor i Danmark, kan hjælpe med at svare på alle spørgsmålene. Det fungere som en almindelig telefonsamtale, men når nogen ringer – ja, så stilles der om til en tilfældig dansker. <br/><br/>Hent vores APP og blive ambassadør for Danmark </p>
                            <div class="download-app-buttons">
                                <a href="#"><img src="resources/apple-dl-button.png" alt=""></a>
                                <a href="#"><img src="resources/play-dl-button.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@endpush