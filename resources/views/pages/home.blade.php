@extends('base')

@section('page')
        <div class="first-section-content">
            <h1>Ring til en dansker</h1>
            <div class="button-wrapper">
                <a href="#bliv-ambassadoer" v-smooth-scroll="{ duration: 1500, offset: -140 }">Bliv ambassadør</a>
                <a href="/download">Download app</a>
            </div>
            <a href="#bliv-ambassadoer" v-smooth-scroll="{ duration: 1500, offset: -140 }"><span class="arrow-down-top"></span></a>
        </div>
        <div class="homepage-hero-module" >
            <div class="video-container">
                <div class="filter"><img src="resources/overlay2.png" alt=""></div>
                <video autoplay loop class="fillWidth"> {{-- autoplay loop --}}
                    <source src="video/Mexican_Bird/Mexican_Bird.mp4" type="video/mp4" />Din browser understøtter ikke baggrundsvideo. Vi foreslår at du opdaterer din browser.
                    <source src="video/Mexican_Bird/Mexican_Bird.webm" type="video/webm" />Din browser understøtter ikke baggrundsvideo. Vi foreslår at du opdaterer din browser.
                </video>
                <div class="poster hidden">
                    <img src="video/Mexican_Bird/Mexican_Bird.jpg" alt="">
                </div>
            </div>
        </div>

        <section id="bliv-ambassadoer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-block">
                            <h2>Svar for Danmark</h2>
                            <p>+45 46 300 400 – et nummer til gode svar på på verdens spørgsmål om Danmark. <br/><br/>Danmark har fået sit eget telefonnummer, som hele verden kan ringe til. Alle danskere kan blive ambassadør for Danmark og hjælpemed at svare for Danmark. <br/><br/>Det fungere som en almindelig samtale, men når nogen ringer, så bliver er det måske dig eller end anden tilfældig dansker der svarer på spørgsmål om alt muligt om danskere og Danamark. <br/><br/>For dig vil det ikke koste noget. Det er en helt almindelig samtale. For den, som ringer til nummeret og derved til dig vil det koste, hvad en lokal samtale koster – den faktiske pris for samtalen afhænger af en række forskellige faktorer, som land, teleselskab, abonnement, tidpunkt på dagen osv.</p>
                            <a href="/download" class="inline-btn">Download app</a>
                        </div>
                    </div>
                    <div class="col-lg-5 col-lg-offset-1">
                        <div class="be-ambassador">
                            <h2>Bliv ambassadør</h2>
                            <hr class="sep">
                            <ambassador></ambassador>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="counter-block">
            <div class="container">
                <div class="left-newsletter-block">
                    <h3>Tilmeld dig vores <b>nyhedsbrev</b>! </h3>
                </div>
                <div class="right-newsletter-block">
                    <div class="signup-container">
                        <input type="email" placeholder="E-mail adresse..">
                        <button class="signup-button">Tilmeld</button>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 graph-title">
                        <h4>Top 7 lande, der ringer</h4>
                        <span>Der er 179 lande, der har ringet til Danmark</span>
                    </div>
                </div>
                <div class="row seven-cols graphs">
                    <div class="col-md-1">
                        <div class="graph">
                            <span class="indicator" style="width: 88%; height: 88%;">
                                <i>31%</i>
                            </span>
                        </div>
                        <b class="country">USA</b>
                    </div>
                    <div class="col-md-1">
                        <div class="graph">
                            <span class="indicator" style="width: 68%; height: 68%;">
                                <i>6%</i>
                            </span>
                        </div>
                        <b class="country">Tyskland</b>
                    </div>
                    <div class="col-md-1">
                        <div class="graph">
                            <span class="indicator" style="width: 58%; height: 58%;">
                                <i>4%</i>
                            </span>
                        </div>
                        <b class="country">Kina</b>
                    </div>
                    <div class="col-md-1">
                        <div class="graph">
                            <span class="indicator" style="width: 58%; height: 58%;">
                                <i>4%</i>
                            </span>
                        </div>
                        <b class="country">Holland</b>
                    </div>
                    <div class="col-md-1">
                        <div class="graph">
                            <span class="indicator" style="width: 58%; height: 58%;">
                                <i>4%</i>
                            </span>
                        </div>
                        <b class="country">Tyrkiet</b>
                    </div>
                    <div class="col-md-1">
                        <div class="graph">
                            <span class="indicator" style="width: 48%; height: 48%;">
                                <i>3%</i>
                            </span>
                        </div>
                        <b class="country">Australien</b>
                    </div>
                    <div class="col-md-1">
                        <div class="graph">
                            <span class="indicator" style="width: 48%; height: 48%;">
                                <i>3%</i>
                            </span>
                        </div>
                        <b class="country">Burkina Faso</b>
                    </div>
                </div>
            </div>
        </section>

        <fact-boxes></fact-boxes>

        @include('pages.footer')

@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
@endpush

@section('script')
<script>
            $.fn.isOnScreen = function(){

            var win = $(window);

            var viewport = {
                top : win.scrollTop(),
                left : win.scrollLeft()
            };
            viewport.right = viewport.left + win.width();
            viewport.bottom = viewport.top + win.height();

            var bounds = this.offset();
            bounds.right = bounds.left + this.outerWidth();
            bounds.bottom = bounds.top + this.outerHeight();

            return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

        };

        $(document).ready(function(){
            $(window).scroll(function(){
                if ($('.graphs').isOnScreen()) {
                    $('.graphs .graph .indicator').addClass('animated zoomIn');
                } else {
                    // run animations again on re-enter
                    //$('.graphs .graph .indicator').removeClass('animated zoomIn');
                }

                if ($('.fact-boxes').isOnScreen()) {
                    $('.fact-boxes .fact-box-wrap').addClass('animated fadeInUp');
                } else {
                    // run animations again on re-enter
                    //$('.fact-boxes .fact-box-wrap').removeClass('animated fadeInUp');
                }
            });
        });
        </script>
@endsection