<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- metas -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
        <meta name="user-id" content="{{ Auth::check() ? Auth::user()->id : '' }}">
 
        <title>Visit Denmark | Ring til en dansker</title>

        <link rel="icon" href="{{ asset('/favicon.png') }}" type="image/x-icon">

        <!-- pushing styles -->
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
        <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
        
        @stack('styles')
        
        <style>#app {display: none;}</style>

        <!-- pushing scripts -->
        @stack('scripts')
    </head>
    <body>
        @include('pages.header')
        <div id="app">
            @yield('page')
            <flash message="{{ session('flash') }}"></flash>
        </div>
    
        {{--  <script src="{{ asset('js/elementui.js') }}"></script>  --}}
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function(){$("#app").fadeIn(700);});
        </script>

        @yield('script')
        <script type="text/javascript" src="{{ asset('js/video.js') }}"></script>
    </body>
</html>
