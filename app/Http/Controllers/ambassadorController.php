<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ambassadorController extends Controller
{
    public function createAmbassador(Request $request) 
    {
        $ambassador = DB::table('ambassadors')->insertGetId([
            'name' => $request->data['name'],
            'email' => $request->data['email'],
            'address' => $request->data['address'],
            'phone_number' => (int)$request->data['phone']
        ]);
        if($ambassador) {
            foreach ($request->languages as $language ) {
                $id = DB::table('languages')
                    ->select('id')
                    ->where('name', '=', $language)
                    ->get();
    
                $result = DB::table('ambassador_to_languages')->insert(['language_id' => (int)$id[0]->id, 'ambassador_id' => (int)$ambassador]);
            }
        }else {
            return response()->json([
                'status' => false
            ]);    
        }

        return response()->json([
            'status' => true,
            'message' => 'Du er nu oprettet som ambassadør'
        ]);    
    }
}
